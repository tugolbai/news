import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { News } from '../../models/news.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {
  news: News[] = [];

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.newsService.getAllNews().subscribe({
      next: news => {
        this.news = news;
      },
      error: error => {
        console.error(error);
      }
    });
  }

  removeNews(id: string) {
    this.newsService.removeNews(id).subscribe(() => {
      this.newsService.getAllNews().subscribe({
        next: news => {
          this.news = news;
        },
        error: error => {
          console.error(error);
        }
      });
    });
  }
}
