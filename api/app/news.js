const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT news.id, news.title, news.image, news.datetime FROM news';
        let [categories] = await db.getConnection()
            .execute(query);
        return res.send(categories);
    } catch (e) {
        next(e);
    }
});


router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.content) {
            return res.status(400).send({message: 'Title and content are required'});
        }

        const news = {
            title: req.body.title,
            content: req.body.content,
            image: null,
            datetime: new Date().toISOString().substr(0,10)
        };

        if (req.file) {
            news.image = req.file.filename;
        }

        let query = 'INSERT INTO news (title, content, image, datetime) VALUES (?, ?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            news.title,
            news.content,
            news.image,
            news.datetime
        ]);

        const id = results.insertId;

        return res.send({message: 'Created news with id = ', id});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [categories] = await db.getConnection().execute('SELECT * FROM news WHERE id = ?', [req.params.id]);

        const category = categories[0];

        if (!category) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(category);
    } catch (e) {
        next(e);
    }
});


router.delete('/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM news WHERE id = ?', [req.params.id]);
        return res.send({message: 'Category with id ' + req.params.id + ' removed'});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

module.exports = router;