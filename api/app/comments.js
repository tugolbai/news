const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT * FROM comments';
        if (req.query.news_id) {
            query += ` WHERE news_id = ${req.query.news_id}`;
        }

        let [comments] = await db.getConnection().execute(query);

        return res.send(comments);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.news_id || !req.body.comment) {
            return res.status(400).send({message: 'NewsID and comment are required'});
        }

        const comment = {
            news_id: req.body.news_id,
            author: null,
            comment: req.body.comment
        };

        if (req.body.author) {
            comment.author = req.body.author;
        }

        let query = 'INSERT INTO comments (news_id, author, comment) VALUES (?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            comment.news_id,
            comment.author,
            comment.comment,
        ]);

        const id = results.insertId;

        return res.send({message: 'Created new comment', id});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM comments WHERE id = ?', [req.params.id]);
        return res.send({message: 'Comment with id ' + req.params.id + ' removed'});

    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

module.exports = router;