import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, NewsData } from '../models/news.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  constructor(private http: HttpClient) { }

  getAllNews() {
    return this.http.get<News[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.content,
            newsData.image,
            newsData.datetime,
          );
        });
      })
    );
  }

  getNews(id: string) {
    return this.http.get<News>(environment.apiUrl + `/news/${id}`).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return new News(id, response.title, response.content, response.image, response.datetime,);
      })
    );
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();

    Object.keys(newsData).forEach(key => {
      if (newsData[key] !== null) {
        formData.append(key, newsData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/news', formData);
  }

  removeNews(id: string) {
    return this.http.delete(environment.apiUrl + '/news/' + `${id}`)
  }
}
