export class News {
  constructor(
    public id: string,
    public title: string,
    public content: string,
    public image: string,
    public datetime: string,
  ) {}
}

export interface NewsData {
  [key: string]: any;
  title: string;
  content: string;
  image: File | null;
  datetime: string;
}
