import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Comment, CommentData } from '../models/comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private http: HttpClient) { }

  getComments(id: string) {
    return this.http.get<Comment[]>(environment.apiUrl + '/comments?news_id=' + id).pipe(
      map(response => {
        return response.map(commentData => {
          return new Comment(
            commentData.id,
            commentData.news_id,
            commentData.author,
            commentData.comment,
          );
        });
      })
    );
  }


  createComment(id: string, commentData: CommentData) {
      const formData = new FormData();

      Object.keys(commentData).forEach(key => {
        if (commentData[key] !== null) {
          formData.append(key, commentData[key]);
        }
      });
      formData.append('news_id', id);

      return this.http.post(environment.apiUrl + '/comments', formData);
  }

  removeComment(id: string) {
    return this.http.delete(environment.apiUrl + '/comments/' + `${id}`)
  }
}
