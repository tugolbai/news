export class Comment {
  constructor(
    public id: string,
    public news_id: string,
    public author: string,
    public comment: string,
  ) {}
}

export interface CommentData {
  [key: string]: any;
  news_id: string;
  author: string;
  comment: string;
}
