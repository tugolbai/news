import { Component, OnInit, ViewChild } from '@angular/core';
import { News } from '../../models/news.model';
import { ActivatedRoute, Data } from '@angular/router';
import { CommentService } from '../../services/comment.service';
import { Comment, CommentData } from '../../models/comment.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.sass']
})
export class NewsDetailsComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  news!: News;
  comments!: Comment[];


  constructor(
    private route: ActivatedRoute,
    private commentService: CommentService,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.news = <News>data.news;
    });

    this.commentService.getComments(this.news.id).subscribe(res =>{
      this.comments = res;
    });
  }

  onSubmit() {
    const commentData: CommentData = this.form.value;
    this.commentService.createComment(this.news.id, commentData).subscribe(() => {
      this.commentService.getComments(this.news.id).subscribe(res =>{
        this.comments = res;
      })
    });
  }

  removeComment(id: string) {
    this.commentService.removeComment(id).subscribe(() => {
      this.commentService.getComments(this.news.id).subscribe(res =>{
        this.comments = res;
      })
    });
  }
}
