import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NewsService } from '../../services/news.service';
import { News } from '../../models/news.model';

@Injectable({
  providedIn: 'root'
})
export class NewsResolverService implements Resolve<News> {

  constructor(private newsService: NewsService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<News> | Observable<never> {
    const newsId = <string>route.params['id'];

    return this.newsService.getNews(newsId).pipe(mergeMap(news => {
      if (news) {
        return of(news);
      }

      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
