import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsComponent } from './pages/news/news.component';
import { EditNewsComponent } from './pages/edit-news/edit-news.component';
import { NewsDetailsComponent } from './pages/news-details/news-details.component';
import { NewsResolverService } from './pages/news/news-resolver.service';

const routes: Routes = [
  {path: '', component: NewsComponent},
  {path: 'news/:id', component: NewsDetailsComponent, resolve: {
      news: NewsResolverService
    }},
  {path: 'posts/new', component: EditNewsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
