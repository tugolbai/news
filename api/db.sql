create schema news collate utf8_general_ci;
use news;

create table news
(
    id       int auto_increment
        primary key,
    title    varchar(255) not null,
    content  text         not null,
    image    varchar(31)  null,
    datetime date         null
);

create table comments
(
    id      int auto_increment
        primary key,
    news_id int          not null,
    author  varchar(100) null,
    comment text         not null,
    constraint comments_news_id_fk
        foreign key (news_id) references news (id)
            on update cascade on delete cascade
);


insert into news.news (id, title, content, image, datetime)
values  (1, 'Olympics: From clothing hauls to TikTok trends, Gen Z Olympians show new side of Games', 'Maddie Mastro of Team USA performs a trick during the women''s snowboard halfpipe qualification on day five of the Beijing 2022 Winter Olympic Games at Genting Snow Park on February 09, 2022 in Zhangjiakou, China.', 'yvVJOldnjwX26X2IZC_jR.jpg', '2022-02-11'),
        (2, 'Kylian Mbappé is hungry for more World Cup success in Qatar', 'French superstar striker Kylian Mbappé tells Becky Anderson that he wants the message of the world''s biggest tournament to be "football is for everyone."', 'qifaoF96rt-2jvhZXAQoS.jpeg', '2022-02-01'),
        (3, 'Kamila Valieva: Russian anti-doping agency allowed teenage figure skater to compete in Olympics despite failed drug test', 'Russian teenage figure skater Kamila Valieva failed a drug test taken in December before the Beijing 2022 Winter Olympics, the International Testing Agency (ITA) confirmed Friday.', null, '2022-02-07');


insert into news.comments (id, news_id, author, comment)
values  (1, 2, 'Tugol', 'COMMENT comment COMMENT comment COMMENT comment'),
        (2, 2, 'Tilek', 'LOREM LOREM LOREM LOREM LOREM'),
        (3, 1, null, 'OOOOOOOHHHHHH, NOOOOO!'),
        (4, 3, 'WhoIs', 'alright'),
        (5, 1, 'boke', 'PSG');